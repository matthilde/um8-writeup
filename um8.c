// UM8 interpreter Linux port
// made by matthilde
#include <stdio.h>
#include <stdlib.h>
typedef unsigned char i8;
typedef unsigned int u32;
#define SWAP(x,y) { tmp = x; x = y; y = tmp; }
int isvalidchr(char c){return (c>='0'&&c<='9')|(c>='A'&&c<='D');}
int main() {
	// define variables;
	i8  a, b, c, tmp, *prgm;
	u32 pc = 0, bufsize, bufmax = 128;
	a = b = c = tmp = 0;

	// read program from stdin
	prgm = (char*)malloc(sizeof(char) * bufmax);
	for (bufsize = 0; isvalidchr(c=getchar()); ++bufsize) {
		if (bufsize >= bufmax)
			prgm = (char*)realloc(prgm, sizeof(char)*(bufmax+=128));
		prgm[bufsize] = c;
	}
	c = 0;

	// run program
	for (;pc < bufsize;++pc)
		switch (prgm[pc]-'0') {
		case 0: return 0;
		case 1: if (b) a = c;
			break;
		case 2: SWAP(a, c); break;
		case 3: a = ~(b&c); break;
		case 4: putchar(b); break;
		case 5: b = getchar(); break;
		case 6: SWAP(a, b); break;
		case 7: a = prgm[pc+c+1] - '0'; break;
		case 8: prgm[pc+a+1] = b + '0'; break;
		case 9: a = prgm[++pc] - '0'; break;
		case 17: pc = b; prgm[pc] = c + '0'; break;
		case 18: a = !a; break;
		case 19: a >>= 1; break;
		case 20: a <<= 1; break;
		}

	return 0;
}
