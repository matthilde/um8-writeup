#!/usr/bin/env python3
import sys
na = "9{a}DDDD29{a}DDDD63"
nb = "69989{a}29{a}6369023"
inter = "2163"
end = "64"

def get_lower_value(b):
    if b in "0123456789": return [b]
    elif b in "ABCDEF":
        n = int(b, 16)
        return [8, n%8]

def do_upper(b):
    return na.format(a = b)
def do_lower(b):
    return nb.format(a = b)
def bt(b):
    return inter + nb.format(a = b)

def do_byte(b):
    u, l = b
    s = do_upper(u)
    l = get_lower_value(l)
    # print(l, file=sys.stderr)
    if len(l) == 2: s += do_lower(l[0]) + bt(l[1])
    else: s += do_lower(l[0])
    s += end
    return s

# print(do_byte(sys.argv[1]))
prev = None
for x in sys.argv[1]:
    hx = hex(ord(x))[2:].upper()
    if hx == prev:
        print('4', end='')
    else:
        print(do_byte(hx), end='')
        prev = hx

print(do_byte('10') + do_byte('13'))
